package com.audiomapp.equalizer_test;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.audiofx.Equalizer;
import android.media.audiofx.Visualizer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.cleveroad.audiovisualization.AudioVisualization;
import com.cleveroad.audiovisualization.DbmHandler;
import com.cleveroad.audiovisualization.VisualizerDbmHandler;

public class MainActivity extends AppCompatActivity {

    public Button Play;
    public Button Stop;
    public Button Equal;
    public Button EqualOff;
    public MediaPlayer myPlayer;
    public Visualizer mVisualizer;
    public Equalizer mEquializer;
    public LinearLayout mLinearLayout;
    public String url;


    public AudioVisualization audioVisualization;

    public long [] Bandfrec = {250,500,1000,2000,4000,8000};

    public int [] Vol = {35,45,50,55,44,70};

    public int [] bandIdT ={1,2,2,3,3,0};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mLinearLayout = (LinearLayout) findViewById(R.id.equalizer);

        Play = (Button) findViewById(R.id.bottonPlay);
        Stop = (Button) findViewById(R.id.buttonStop);
        Stop.setVisibility(View.INVISIBLE);

        Equal = (Button) findViewById(R.id.buttonEqualiz);
        EqualOff = (Button) findViewById(R.id.buttonEqualOff);
        EqualOff.setVisibility(View.INVISIBLE);



        audioVisualization = (AudioVisualization) findViewById(R.id.visualizer_view);

        Play.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Perform action on click
           play();
           Play.setVisibility(View.INVISIBLE);
           Stop.setVisibility(View.VISIBLE);

            }
        });

        Stop.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Perform action on click
         stop();
         Stop.setVisibility(View.INVISIBLE);
         Play.setVisibility(View.VISIBLE);

            }
        });

        Equal.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Perform action on click
                equalizando();
                Equal.setVisibility(View.INVISIBLE);
                EqualOff.setVisibility(View.VISIBLE);
                //mLinearLayout.setVisibility(View.VISIBLE);

            }
        });

        EqualOff.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Perform action on click
                mEquializer.setEnabled(false);
                EqualOff.setVisibility(View.INVISIBLE);
                Equal.setVisibility(View.VISIBLE);
                //mLinearLayout.setVisibility(View.INVISIBLE);

            }
        });

    }

    public void play () {

        // hace visible el equalizador

        //url = "http://sverigesradio.se/sida/avsnitt/826695?programid=411";
        //url = "http://programmerguru.com/android-tutorial/wp-content/uploads/2013/04/hosannatelugu.mp3";
        //url = "http://sverigesradio.se/topsy/direkt/132-lo.mp3";

        setVolumeControlStream(AudioManager.STREAM_MUSIC);

        // player archivo
        //myPlayer = MediaPlayer.create(this, R.raw.estaciones);
        myPlayer = MediaPlayer.create(this, R.raw.lifeforrent);

        // M player para Streaming
        /*
        myPlayer = new MediaPlayer();
        myPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);

        try {
            myPlayer.setDataSource(url);
        } catch (IllegalArgumentException e) {
            Toast.makeText(getApplicationContext(), "You might not set the URI correctly!", Toast.LENGTH_LONG).show();
        } catch (SecurityException e) {
            Toast.makeText(getApplicationContext(), "You might not set the URI correctly!", Toast.LENGTH_LONG).show();
        } catch (IllegalStateException e) {
            Toast.makeText(getApplicationContext(), "You might not set the URI correctly!", Toast.LENGTH_LONG).show();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            myPlayer.prepare();
        } catch (IllegalStateException e) {
            Toast.makeText(getApplicationContext(), "You might not set the URI correctly!", Toast.LENGTH_LONG).show();
        } catch (IOException e) {
            Toast.makeText(getApplicationContext(), "You might not set the URI correctly!", Toast.LENGTH_LONG).show();
        }
        */

        myPlayer.start();

        // Visualizador
        VisualizerDbmHandler vizhand = DbmHandler.Factory.newVisualizerHandler(this, myPlayer);
        audioVisualization.linkTo(vizhand);
        audioVisualization.onResume();

        // Cuando termina de tocar el tema
        myPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                mEquializer.setEnabled(false);

                Play.setVisibility(View.VISIBLE);
                Stop.setVisibility(View.INVISIBLE);

                Equal.setVisibility(View.VISIBLE);
                EqualOff.setVisibility(View.INVISIBLE);

                audioVisualization.release();

            }


        });


    }

    public void stop () {

        myPlayer.stop();

        //audioVisualization.release();
        audioVisualization.onPause();


    }

    public void Visual() {
        /*
        new GLAudioVisualizationView.Builder(Context())
                .setBubblesSize(R.dimen.bubble_size)
                .setBubblesRandomizeSize(true)
                .setWavesHeight(R.dimen.wave_height)
                .setWavesFooterHeight(R.dimen.footer_height)
                .setWavesCount(7)
                .setLayersCount(4)
                .setBackgroundColorRes(R.color.av_color_bg)
                .setLayerColors(R.array.av_colors)
                .setBubblesPerLayer(16)
                .build();
          */

    }

    public void equalizando() {

        mEquializer = new Equalizer(0,myPlayer.getAudioSessionId());

        //mEquializer.getProperties().numBands
        //mEquializer.getNumberOfBands();
        //mEquializer.getNumberOfPresets();
        Log.v("equalizer num bandas", String.valueOf(mEquializer.getNumberOfBands()));
        Log.v("equalizer Presets", String.valueOf(mEquializer.getNumberOfPresets()));
        //mEquializer.getBand(500);
        Log.v("frec 250 ", String.valueOf(mEquializer.getBand(250*1000)));
        Log.v("frec 500 ", String.valueOf(mEquializer.getBand(500*1000)));
        Log.v("frec 1000 ", String.valueOf(mEquializer.getBand(1000*1000)));
        Log.v("frec 2000 ", String.valueOf(mEquializer.getBand(2000*1000)));
        Log.v("frec 4000 ", String.valueOf(mEquializer.getBand(4000*1000)));
        Log.v("frec 8000 ", String.valueOf(mEquializer.getBand(8000*1000)));


        // Config segun audiom
        // frec 250 vol 5dB
        mEquializer.setBandLevel((short) 1, (short) 500);
        // frec 500 vol 7dB
        mEquializer.setBandLevel((short) 2, (short) 700);
        // frec 1000 vol 7dB
        mEquializer.setBandLevel((short) 2, (short) 700);
        // frec 2000 vol 8dB
        mEquializer.setBandLevel((short) 3, (short) 800);
        // frec 4000 vol 7dB
        mEquializer.setBandLevel((short) 3, (short) 700);
        // frec 8000 vol 10dB
        mEquializer.setBandLevel((short) 0, (short) 1000);


        mEquializer.setEnabled(true);

        //setupEqualizerFxAndUI();

        EqualizerBarr();

    }
    public void setupEqualizerFxAndUI() {

        mLinearLayout = (LinearLayout) findViewById(R.id.equalizer);

        // headings
        TextView equalHeading = new TextView(this);
        equalHeading.setText("Equalizer");
        equalHeading.setTextSize(20);
        equalHeading.setGravity(Gravity.CLIP_HORIZONTAL);
        mLinearLayout.addView(equalHeading);

        // num frec soportados
        short numberFrecBands = mEquializer.getNumberOfBands();


        final short lowEqualBandLevel = mEquializer.getBandLevelRange()[0];

        final short upEqualBandLevel = mEquializer.getBandLevelRange()[1];



        for (short i = 0; i < numberFrecBands; i++ ) {

            final short equalizerBandIndex = i;

            // display center frequency
            TextView frecHeaderTextView = new TextView(this);

            frecHeaderTextView.setLayoutParams(new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT
            ));
            frecHeaderTextView.setGravity(Gravity.CENTER_HORIZONTAL);
            frecHeaderTextView.setText((mEquializer.getCenterFreq(equalizerBandIndex)/1000) + " Hz");
            mLinearLayout.addView(frecHeaderTextView);

            // Setting up the layouts for Seekbar sliders
            LinearLayout seekbarRowLayout = new LinearLayout(this);

            seekbarRowLayout.setOrientation(LinearLayout.HORIZONTAL);

            // Text abajo
            TextView lowerEqualBandLevelTextView = new TextView(this);

            lowerEqualBandLevelTextView.setLayoutParams(new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT));

            lowerEqualBandLevelTextView.setText((lowEqualBandLevel / 100) + " dB");

            // Texto arriba
            TextView upperEqualBandLevelTextview = new TextView(this);
            upperEqualBandLevelTextview.setLayoutParams(new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT));
            upperEqualBandLevelTextview.setText((upEqualBandLevel / 100) + " dB");

            // Creatingthe SeekBar Sliders
            // set the layout param for seekbar
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            layoutParams.weight = 1;

            // create a new SeekBAr
            SeekBar seekBar = new SeekBar(this);
            // give the seekBar an ID
            seekBar.setId(i);

            seekBar.setLayoutParams(layoutParams);
            seekBar.setMax(upEqualBandLevel - lowEqualBandLevel);
            // set the progress for this seekBar
            seekBar.setProgress(mEquializer.getBandLevel(equalizerBandIndex));

            // change progress as its changed by moving the sliders
            seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mEquializer.setBandLevel(equalizerBandIndex,(short) (progress + lowEqualBandLevel));

            }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {
                    // not used

                }


            public void onStopTrackingTouch(SeekBar seekbar) {
                // not used

            }

            });

            // add lower upper band level text view
            seekbarRowLayout.addView(lowerEqualBandLevelTextView);
            seekbarRowLayout.addView(seekBar);
            seekbarRowLayout.addView(upperEqualBandLevelTextview);

            mLinearLayout.addView(seekbarRowLayout);

            // show the spinner


        }



    }


    public void EqualizerBarr() {

        mLinearLayout = (LinearLayout) findViewById(R.id.barraSeleccion);

        // headings
        TextView equalHeading = new TextView(this);
        equalHeading.setText("Equalizer 1");
        equalHeading.setTextSize(20);
        equalHeading.setGravity(Gravity.CLIP_HORIZONTAL);
        mLinearLayout.addView(equalHeading);

        // num frec soportados
        //short numberFrecBands = mEquializer.getNumberOfBands();
        short numberFrecBands = 6;

        //final short lowEqualBandLevel = mEquializer.getBandLevelRange()[0];
        final short lowEqualBandLevel = 0;

        //final short upEqualBandLevel = mEquializer.getBandLevelRange()[1];
        final short upEqualBandLevel = 100;



        for (short i = 0; i < numberFrecBands; i++ ) {

            final short equalizerBandIndex = i;

            // display center frequency
            TextView frecHeaderTextView = new TextView(this);

            frecHeaderTextView.setLayoutParams(new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT
            ));
            frecHeaderTextView.setGravity(Gravity.CENTER_HORIZONTAL);
            frecHeaderTextView.setText(String.valueOf(Bandfrec[equalizerBandIndex]) + " Hz");

            mLinearLayout.addView(frecHeaderTextView);

            // Setting up the layouts for Seekbar sliders
            LinearLayout seekbarRowLayout = new LinearLayout(this);

            seekbarRowLayout.setOrientation(LinearLayout.HORIZONTAL);

            // Text abajo
            TextView lowerEqualBandLevelTextView = new TextView(this);

            lowerEqualBandLevelTextView.setLayoutParams(new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT));

            lowerEqualBandLevelTextView.setText((lowEqualBandLevel) + " dB");

            // Texto arriba (derecha)
            final TextView upperEqualBandLevelTextview = new TextView(this);
            upperEqualBandLevelTextview.setLayoutParams(new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT));
            //upperEqualBandLevelTextview.setText((upEqualBandLevel) + " dB");
            upperEqualBandLevelTextview.setText((Vol[equalizerBandIndex]) + " dB");

            // Creatingthe SeekBar Sliders
            // set the layout param for seekbar
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            layoutParams.weight = 1;

            // create a new SeekBAr
            SeekBar seekBar = new SeekBar(this);
            // give the seekBar an ID
            seekBar.setId(i);

            seekBar.setLayoutParams(layoutParams);
            seekBar.setMax(upEqualBandLevel - lowEqualBandLevel);
            // set the progress for this seekBar
            //seekBar.setProgress(mEquializer.getBandLevel(equalizerBandIndex));
            seekBar.setProgress(Vol[equalizerBandIndex]);

            // change progress as its changed by moving the sliders
            seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    //mEquializer.setBandLevel(equalizerBandIndex,(short) (progress + lowEqualBandLevel));
                    Log.v("equalizer progress", String.valueOf(progress));
                    Vol[equalizerBandIndex] = progress;

                    upperEqualBandLevelTextview.setText((Vol[equalizerBandIndex]) + " dB");

                    //progressnew = int(progress*15/100);
                    float a=(progress*15)/100;
                    int b;

                    b = (int)a;

                    // asigna volumen nuevo a la banda segun el indice
                    mEquializer.setBandLevel((short) bandIdT[equalizerBandIndex],(short) (b));
                       Log.v("equalizer newvol", String.valueOf(b));
                    Log.v("equalizer band", String.valueOf(bandIdT[equalizerBandIndex]));

                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {
                    // not used


                }


                public void onStopTrackingTouch(SeekBar seekbar) {
                    // not used

                }

            });

            // add lower upper band level text view
            seekbarRowLayout.addView(lowerEqualBandLevelTextView);
            seekbarRowLayout.addView(seekBar);
            seekbarRowLayout.addView(upperEqualBandLevelTextview);

            mLinearLayout.addView(seekbarRowLayout);




        }


    }


}
